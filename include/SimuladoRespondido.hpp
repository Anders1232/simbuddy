#ifndef SIMULADO_RESPONDIDO_HPP
#define SIMULADO_RESPONDIDO_HPP

#include<vector>
#include<string>
#include<array>
#include "CertezaQuestao.hpp"
#include "Defines.hpp"



class SimuladoRespondido
{
	public:
		SimuladoRespondido(void);
		float GetNota(void) const;

		CERTEZA_QUESTAO ObterCerteza(uint questao) const;
		MARCACAO_VF ObterMarcacao(uint questao) const;
		bool EmBranco(uint questao) const;
		bool FoiAnulada(uint questao) const;
		MARCACAO_VF ObterGabarito(uint questao) const;
//	private:
		uint idSimulado;

		std::array<uchar, SIM_ARRAY_LENGHT_BYTES> marcacao;
		std::array<uchar, SIM_ARRAY_LENGHT_BYTES> brancos;
		std::array<uchar, 2*SIM_ARRAY_LENGHT_BYTES> certeza;
//levar os itens abaixo para a classe do simulado?
		std::array<uchar, SIM_ARRAY_LENGHT_BYTES> anuladas;
		std::array<uchar, SIM_ARRAY_LENGHT_BYTES> gabarito;
};

#endif
