#ifndef QUESTAO_HPP
#define QUESTAO_HPP

#include <vector>
#include <string>

enum TipoQuestao{
    VAZIO,
    CE,
    ABCD,
    ABCDE,
    DEFINIDO_EM_TAG,
    TIPO_QUESTAO_COUNT
};

class Questao{
	public:
	private:
		int id;
        TipoQuestao tipoQuestao;
//		int idDisciplina;
//		int idProfessor;
//		int idSimulado;
//        int idTextoAssociado;
        //Colocar muitas features do sistema como tags torna a persistência mais preparada para o futuro
        //Aí se utilizaria tags com info do tipo chave-valor para muitas funcionalidades
		std::vector<int> idTags;

		std::string textoQuestao;
        std::vector<std::string> textoAlternativas;
        char gabarito;
        //DATA ultimaVezQuestaoRespondida
};

#endif
