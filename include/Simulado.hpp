#ifndef SIMULADO_HPP
#define SIMULADO_HPP

#include <vector>
#include <string>
#include <time.h>
#include "Defines.hpp"

class Simulado
{
	public:
	private:
		int idEntidade;
		int idCargo;
		int idEmpresa;
		std::vector<int> idQuestoes;
//		std::vector<int> simTags;
		time_t dataSimulado;

		std::vector<std::string> materias;
		std::vector<std::string> professoresMaterias;
		std::vector<uchar[SIM_ARRAY_LENGHT_BYTES]> mascaraMaterias;

};

#endif
