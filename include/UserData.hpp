#ifndef USER_DATA_HPP
#define USER_DATA_HPP

#include "Empresa.hpp"
#include "Entidade.hpp"
#include "Questao.hpp"
#include "Simulado.hpp"
#include "Cargo.hpp"
#include "SimuladoRespondido.hpp"
#include "Tag.hpp"
#include "Professor.hpp"
#include <vector>

class UserData{
	public:
//	private:
		std::vector<Empresa> empresas;
		std::vector<Entidade> entidades;
		std::vector<Questao> questoes;
		std::vector<Simulado> simulados;
		std::vector<Cargo> cargos;
		std::vector<SimuladoRespondido> simuladosRespondidos;
		std::vector<Tag> tags;
		std::vector<Professor> professores;
};

#endif
