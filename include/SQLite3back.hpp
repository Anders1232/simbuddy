#ifndef SQLITE_3_BACK_HPP
#define SQLITE_3_BACK_HPP

//#include <sqlite3.h>
#include <sqlite/command.hpp>
#include <sqlite/connection.hpp>
#include <sqlite/execute.hpp>
#include <sqlite/query.hpp>
#include <sqlite/result.hpp>
#include <sqlite/transaction.hpp>
#include <sqlite/database_exception.hpp>
#include <string>
#include "UserData.hpp"


class SQLite3Back
{
	public:
//		LoadSimulados(std::string arquivo);
//		LoadMarcacoes(std::string arquivo);
		static UserData Create(std::string const caminho);
		static UserData Load(std::string const caminho);
//		Load(std::string simulados, std::string marcacoes);
	private:
		static void CreateDB(sqlite::connection& con);
//		sqlite::connection dbSimulados;
//		sqlite::connection dbMarcacoes;
};



#endif

