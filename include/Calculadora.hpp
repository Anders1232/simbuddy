#ifndef CALCULADORA_HPP
#define CALCULADORA_HPP

#include <array>
#include <tuple>
#include "UserData.hpp"
#include "CertezaQuestao.hpp"

class AnaliseSimulado
{
	public:
		AnaliseSimulado(void);
		float nota;
		uchar acertos;
		uchar erros;
		uchar anuladas;
		uchar brancos;

		/*
		 * A matriz abaixo é feita para ser usada da seguinte forma: contagemMarcaçoes[MARQUEI_C_ACERTEI][CHUTE_ACHISMO] e a resposta é o número de questóes nessa classificação
		*/

		std::array <std::array <int, static_cast<std::size_t>(RESULTADO::SIZE)>, static_cast<std::size_t>(CERTEZA_QUESTAO::SIZE)> contagemMarcacoes;
	//	std::array <std::tuple <MARCACAO_VF, CERTEZA_QUESTAO>, SIM_NUM_QUESTOES> marcacoes;


	private:
		void InicializarContagemMarcacoes(void);

};


class Calculadora
{
	public:
		static AnaliseSimulado CalcularNota(UserData& data, uint const idSimulado);
};

#endif
