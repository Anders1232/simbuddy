#ifndef CERTEZA_QUESTAO_HPP
#define CERTEZA_QUESTAO_HPP

enum class CERTEZA_QUESTAO{
	GARANTIDO=0,
	DUVIDA,
	CHUTE_ACHISMO,
	BRANCO_ACHISMO,
	SIZE
};


enum class MARCACAO_VF{
	FALSO=0,
	VERDADEIRO,
	SIZE
};

enum class RESULTADO{
	MARQUEI_C_ACERTEI=0,
	MARQUEI_C_ERREI,
	MARQUEI_E_ACERTEI,
	MARQUEI_E_ERREI,
	SIZE
};

#endif
