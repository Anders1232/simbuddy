#include <QFileDialog>
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "SQLite3back.hpp"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
}


void MainWindow::on_actionNovo_triggered()
{
	QString novoArq= QFileDialog::getSaveFileName(this, tr("Novo Aluno"));
	SQLite3Back::Create(novoArq.toStdString() );
}
