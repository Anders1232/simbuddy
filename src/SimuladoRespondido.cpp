#include "SimuladoRespondido.hpp"

CERTEZA_QUESTAO SimuladoRespondido::ObterCerteza(uint questao) const
{
	questao*= 2; // com isso identificamos a posição no vetor que queremos analisar
	uint indiceByte=questao/8;
	/*
	 *  a operação abaixo foi definida com objetivo deter os seguintes resultados
	 *  se for para pegar as primeiras questões do byte, tem que pegar os bit de mais alta ordem
	 *  se for para pegar as últimas questões do byte, tem que pegar os bit de mais baixa ordem
	 *  F(0)= 0xC0
	 *  F(1)= 0x30
	 *  F(2)= 0x0C
	 *  F(3)= 0x03
	 *  onde a variável dessa função é metade do resto
	 */
	uint deslocamentoPraMascara= (6 - questao%8);
	uint mascaraBits= 0x3 << deslocamentoPraMascara;
	uchar byte= certeza[indiceByte] & mascaraBits;
	return static_cast<CERTEZA_QUESTAO>( byte >> deslocamentoPraMascara); // a principio era pra ser o bastante mas compilador ta de mimimi
}

MARCACAO_VF SimuladoRespondido::ObterMarcacao(uint questao) const
{
	uint indiceByte=questao/8;
	int posicaoNoByte= questao%8;
	int mascaraByte= 0x1 << (7-posicaoNoByte);
	if(marcacao[indiceByte] & mascaraByte)
	{
		return MARCACAO_VF::VERDADEIRO;
	}
	else
	{
		return MARCACAO_VF::FALSO;
	}
}

bool SimuladoRespondido::EmBranco(uint questao) const
{
	uint indiceByte=questao/8;
	int posicaoNoByte= questao%8;
	uint mascaraByte= 0x1 << (7-posicaoNoByte);
	return brancos[indiceByte] & mascaraByte;
}

bool SimuladoRespondido::FoiAnulada(uint questao) const
{
	uint indiceByte=questao/8;
	int posicaoNoByte= questao%8;
	uint mascaraByte= 0x1 << (7-posicaoNoByte);
	return anuladas[indiceByte] & mascaraByte;
}

MARCACAO_VF SimuladoRespondido::ObterGabarito(uint questao) const
{
	uint indiceByte=questao/8;
	int posicaoNoByte= questao%8;
	int mascaraByte= 0x1 << (7-posicaoNoByte);
	if(gabarito[indiceByte] & mascaraByte)
	{
		return MARCACAO_VF::VERDADEIRO;
	}
	else
	{
		return MARCACAO_VF::FALSO;
	}
}





