#include "Calculadora.hpp"
#include <string.h>
#include <assert.h>

AnaliseSimulado::AnaliseSimulado(){}

AnaliseSimulado Calculadora::CalcularNota(UserData& data, uint const idSimulado)
{
	SimuladoRespondido &respostas = data.simuladosRespondidos.at(static_cast<uint>(idSimulado) );
	AnaliseSimulado res;

	for(uint i =0; i < SIM_NUM_QUESTOES; i++)
	{
		if(respostas.FoiAnulada(i))
		{
			res.anuladas++;
		}
		else if(respostas.EmBranco(i))
		{
				res.brancos++;
		}
		else
		{
			if(respostas.ObterMarcacao(i) == respostas.ObterGabarito(i))
			{
				res.acertos++;
				std::array<int, static_cast<std::size_t>(RESULTADO::SIZE)> &aux= res.contagemMarcacoes[static_cast<uint>(respostas.ObterCerteza(i) )];
				if(MARCACAO_VF::VERDADEIRO == respostas.ObterMarcacao(i) )
				{
					aux[static_cast<uint>(RESULTADO::MARQUEI_C_ACERTEI)]++;
				}
				else{
					aux[static_cast<uint>(RESULTADO::MARQUEI_E_ACERTEI)]++;
				}
			}
			else
			{
				res.erros++;
				std::array<int, static_cast<std::size_t>(RESULTADO::SIZE)> &aux= res.contagemMarcacoes[static_cast<uint>(respostas.ObterCerteza(i) )];
				if(MARCACAO_VF::VERDADEIRO == respostas.ObterMarcacao(i) )
				{
					aux[static_cast<uint>(RESULTADO::MARQUEI_C_ERREI)]++;
				}
				else{
					aux[static_cast<uint>(RESULTADO::MARQUEI_E_ERREI)]++;
				}
			}
		}
	}
	return res;
}

void AnaliseSimulado::InicializarContagemMarcacoes(void)
{
//	std::array <std::array <int, static_cast<std::size_t>(RESULTADO::SIZE)>, static_cast<std::size_t>(CERTEZA_QUESTAO::SIZE)>& contagemMarcacoes= res.contagemMarcacoes;

	contagemMarcacoes[(int)CERTEZA_QUESTAO::GARANTIDO][(int)RESULTADO::MARQUEI_C_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::GARANTIDO][(int)RESULTADO::MARQUEI_C_ERREI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::GARANTIDO][(int)RESULTADO::MARQUEI_E_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::GARANTIDO][(int)RESULTADO::MARQUEI_E_ERREI]= 0;

	contagemMarcacoes[(int)CERTEZA_QUESTAO::DUVIDA][(int)RESULTADO::MARQUEI_C_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::DUVIDA][(int)RESULTADO::MARQUEI_C_ERREI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::DUVIDA][(int)RESULTADO::MARQUEI_E_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::DUVIDA][(int)RESULTADO::MARQUEI_E_ERREI]= 0;

	contagemMarcacoes[(int)CERTEZA_QUESTAO::CHUTE_ACHISMO][(int)RESULTADO::MARQUEI_C_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::CHUTE_ACHISMO][(int)RESULTADO::MARQUEI_C_ERREI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::CHUTE_ACHISMO][(int)RESULTADO::MARQUEI_E_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::CHUTE_ACHISMO][(int)RESULTADO::MARQUEI_E_ERREI]= 0;

	contagemMarcacoes[(int)CERTEZA_QUESTAO::BRANCO_ACHISMO][(int)RESULTADO::MARQUEI_C_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::BRANCO_ACHISMO][(int)RESULTADO::MARQUEI_C_ERREI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::BRANCO_ACHISMO][(int)RESULTADO::MARQUEI_E_ACERTEI]= 0;
	contagemMarcacoes[(int)CERTEZA_QUESTAO::BRANCO_ACHISMO][(int)RESULTADO::MARQUEI_E_ERREI]= 0;
}
