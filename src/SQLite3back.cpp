#include "SQLite3back.hpp"

UserData SQLite3Back::Create(const std::string caminho)
{
	sqlite::connection con (caminho);
	CreateDB(con);
	return UserData();
}

void SQLite3Back::CreateDB(sqlite::connection& con)
{
	sqlite::execute(con,
					"CREATE TABLE TB_TAG("
					"ID NOT NULL PRIMARY KEY AUTOINCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_CARGO("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_EMPRESA("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_ENTIDADE("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_PROFESSOR("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_DISCIPLINA("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" NOME NOT NULL VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_QUESTAO("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" FK_ID_PROFESSOR int FOREIGN KEY REFERENCES TB_PROFESSOR(ID),"
					" FK_ID_DISCIPLINA int FOREIGN KEY REFERENCES TB_DISCIPLINA(ID))",
					true);
	sqlite::execute(con,
					"CREATE_TABLE TB_TAG_QUESTOES("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" FK_ID_TAG int NOT NULL FOREIGN KEY REFERENCES TB_TAG(ID),"
					" FK_ID_QUESTAO int NOT NULL FOREIGN KEY REFERENCES TB_QUESTAO(ID),"
					" TEXTO VARCHAR(255))",
					true);
	sqlite::execute(con,
					"CREATE TABLE TB_SIMULADO("
					"ID NOT NULL PRIMARY KEY AUTO INCREMENT int,"
					" FK_ID_ENTIDADE FOREIGN KEY REFERENCES TB_ENTIDADE(ID) NOT NULL int,"
					" FK_ID_EMPRESA NOT NULL REFERENCES TB_EMPRESA(ID) int,"
					" FK_ID_CARGO NOT NULL int FOREIGN KEY REFERENCES TB_CARGO(ID) )",
					true);
	sqlite::execute(con,
					"CREATE_TABLE TB_SIMULADO_QUESTOES("
					"ID NOT_NULL PRIMARY KEY AUTO INCREMENT int"
					" FK_ID_SIMULADO FOREIGN KEY REFERENCES TB_SIMULADO(ID) int,"
					" NUM_QUESTAO NOT NULL int,"
					" FK_ID_QUESTAO NOT NULL FOREIGN KEY REFERENCES TB_QUESTAO(ID) )",
					true);

}

UserData SQLite3Back::Load(std::string const caminho)
{
	sqlite::connection dbConnection(caminho);
//	sqlite::execute(dbConnection, "").
	sqlite::query query(dbConnection, "SELECT * FROM TB_EMPRESAS");
	sqlite::result_type res= query.emit_result();
	assert(2 == res->get_column_count());

//	comando.
}
